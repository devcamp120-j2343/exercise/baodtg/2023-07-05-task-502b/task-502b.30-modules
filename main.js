// import { Rectangle, name, address } from "./rectangle.js";
import Rectangle from "./rectangle.js";
import  Square  from "./square.js";
let rec1 = new Rectangle(10, 5);
console.log(`get length: ${rec1.length}`)
console.log(`get width: ${rec1.width}`)
console.log(`get area: ${rec1.getArea()}`)


let square1 = new Square(10, 10);
console.log(square1.length)
console.log(square1.width)
console.log(square1.getArea())
console.log(square1.perimeter)
console.log(square1.getPerimeter())

console.log(rec1 instanceof Rectangle)
console.log(square1 instanceof Rectangle)
console.log(name)