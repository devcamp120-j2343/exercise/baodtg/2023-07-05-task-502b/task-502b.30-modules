import Rectangle from "./rectangle.js";

class Square extends Rectangle {
    constructor(width) {
        super(width, width)
        // this.width = width;
        // this.length = width;
        this.perimeter = width * 4
    }
    getPerimeter() {
        return this.perimeter
    }
}
export default
    Square
