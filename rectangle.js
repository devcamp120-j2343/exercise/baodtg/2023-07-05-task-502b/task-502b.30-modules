 //tạo một class:
class Rectangle {
    //hàm tạo (constructor):
    constructor(length, width) {
        this.length = length;
        this.width = width;
    }
    //phương thức class:
    getArea() {
        return this.length * this.width;
    }
} 
let name = "Bao Dinh";
let address = "quan 8"
export {Rectangle, name, address};
export default Rectangle